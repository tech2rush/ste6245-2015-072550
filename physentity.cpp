#include "physentity.h"

bool PhysEntity::staticObject() const
{
    return _staticObject;
}

void PhysEntity::setStaticObject(bool s)
{
    _staticObject = s;
}
