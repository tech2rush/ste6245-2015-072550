#ifndef PLANE_H
#define PLANE_H

#include <gmCoreModule>
#include <parametrics/gmpplane>
#include "physentity.h"

class Plane: public PhysEntity
{
public:
    Plane(const GMlib::Point<float,3>& origin, const GMlib::Vector<float,3>& normal):
        _origin(origin), _normal(normal){}

    void update(float dt) override{}

    bool collide(const PhysEntity* pe, float& t) const override;
    void impact(PhysEntity* pe) override;

    bool collideV(const Ball* ball, float& t) const;
    bool collideV(const Plane* plane, float& t) const;
    bool collideV(const Triangle* triangle, float& t) const override;

    void impactV(Ball* ball) const override;
    void impactV(Plane* plane) const override;
    void impactV(Triangle* triangle) const override;

    void applyGravity(float dt) override{}

    GMlib::Vector<float, 3> normal() const;
    void setNormal(const GMlib::Vector<float, 3> &normal);

    GMlib::Point<float, 3> origin() const;
    void setOrigin(const GMlib::Point<float, 3> &origin);

    float friction() const;
    void setFriction(float friction);

private:
    GMlib::Point<float,3> _origin;
    GMlib::Vector<float,3> _normal;

    float _friction = 1.2;

};

#endif
