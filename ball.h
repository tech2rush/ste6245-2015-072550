#ifndef BALL_H
#define BALL_H

#include <gmParametricsModule>
#include "physentity.h"

class Ball : public PhysEntity, public GMlib::PSphere<float>
{
public:
   Ball(const GMlib::Point<float,3>& position,
        const GMlib::Vector<float,3>& velocity,
        float radius = 1,
        float mass = 1);
   ~Ball();
   bool collide(const PhysEntity* pe, float& t) const override;
   void impact(PhysEntity* pe) override;

   void update(float dt) override;
   void localSimulate(float dt);

   bool collideV(const Ball* ball, float& t) const override;
   bool collideV(const Plane* plane, float& t) const override;
   bool collideV(const Triangle* triangle, float& t) const override;

   void impactV(Ball* ball) const override;
   void impactV(Plane* plane) const override;
   void impactV(Triangle* triangle) const override;

   void applyGravity(float dt) override;

   GMlib::Point<float, 3> velocity() const;
   void setVelocity(const GMlib::Point<float, 3> &velocity);

   GMlib::Point<float, 3> position() const;

   float mass() const;
   void setMass(float mass);

   bool rolling() const;
   void setRolling(bool rolling);

   float friction() const;
   void setFriction(float friction);

protected:
   GMlib::Point<float,3> _velocity;
   float _mass;
   bool _rolling;
   float _friction = 1.2;
};



#endif
