#ifndef COLLISION_H
#define COLLISION_H

#include <gmCoreModule>

class Ball;
class Plane;

struct Collision
{


    static const GMlib::Vector<float,3> gravity;
    static constexpr float epsilon = 1e-5;

    static bool collide(const Ball* b, const Plane* p, float& t);
    static bool collide(const Ball* b1, const Ball* b2, float& t);

    static void impact(Ball* b, const Plane* p);
    static void impact(Ball* b1, Ball* b2);
};
#endif//COLLISION_H
