#include "collision.h"
#include "plane.h"
#include "ball.h"

const GMlib::Vector<float,3> Collision::gravity {0.0f, 0.0f, -9.81f};

bool Collision::collide(const Ball *b, const Plane *p, float &t)
{
    //Check for parallell movement
    auto div = p->normal() * b->velocity();

 // Parallell movement or moving away //Should be < , something wrong here, why does this work?
    //is p->normal returning the correct value?
    if (div > epsilon)
    {
        return false;
    }

    //Find impact time
    t = (p->normal() * (p->origin() - b->position()) + b->getRadius()) / div;//Should be -radius

    if (t < 0 ) t = epsilon;

    return true;
}

bool Collision::collide(const Ball *b1, const Ball *b2, float &t)
{
    auto deltaV = b1->velocity() - b2->velocity();

    auto sumR = b1->getRadius() + b2->getRadius();

    auto deltaP = b1->position() - b2->position();

    auto a = deltaV * deltaV;
    auto b = 2 * deltaP * deltaV;
    auto c = deltaP * deltaP - sumR * sumR;
    float d = b * b - 4 * a * c;

    //solve quadratic
    if (a == 0 || d < 0 || b > 0) return false; //divide by zero, sqrt(-x), negative result

    t = (-b -sqrt(d)) / ( 2 * a);
    if (t < 0.0) t = epsilon;
    return true;
}


//Todo: comment this
void Collision::impact(Ball *b, const Plane *p)
{
    auto normal = p->normal().normalize();

    float div = p->normal() * b->velocity();


    b->setRolling(false);

    if (div > -1.5)
    {
        b->setVelocity(b->velocity() - div * p->normal());
        if (normal * gravity != 0) b->setRolling(true);
    }
    else b->setVelocity(b->velocity() - 1.7 * div * p->normal());
    return;
}

//http://nehe.gamedev.net/tutorial/collision_detection/17005/
void Collision::impact(Ball *b1, Ball *b2)
{
    b1->setRolling(false);
    b2->setRolling(false);

    float m1 = b1->mass();
    float m2 = b2->mass();

    GMlib::Vector<float,3> x = b1->position() - b2->position();
    x.normalize();

    GMlib::Vector<float,3> u1 = b1->velocity() * x;
    GMlib::Vector<float,3> u2 = b1->velocity() * x;

    GMlib::Vector<float,3> v1 = ((u1 * m1) + (u2 * m2) - (u1 -u2) * m2) / (m1 + m2);
    GMlib::Vector<float,3> v2 = ((u1 * m1) + (u2 * m2) - (u2 -u1) * m1) / (m1 + m2);

    b1->velocity() += (v1 - u1) * x;
    b1->velocity() += (v2 - u2) * x;
}
