#include "plane.h"
#include "collision.h"

bool Plane::collide(const PhysEntity *pe, float &t) const
{
    return pe->collideV(this, t);
}

void Plane::impact(PhysEntity *pe)
{
    pe->impactV(this);
}

bool Plane::collideV(const Ball *ball, float &t) const
{
    return Collision::collide(ball, this, t);
}

bool Plane::collideV(const Plane *plane, float &t) const
{
    return false;
}

bool Plane::collideV(const Triangle *triangle, float &t) const
{

}

void Plane::impactV(Ball *ball) const
{
   Collision::impact(ball, this);
}

void Plane::impactV(Plane *plane) const
{
}

void Plane::impactV(Triangle *triangle) const
{

}

GMlib::Vector<float, 3> Plane::normal() const
{
    return _normal;
}

void Plane::setNormal(const GMlib::Vector<float, 3> &normal)
{
    _normal = normal;
}

GMlib::Point<float, 3> Plane::origin() const
{
    return _origin;
}

void Plane::setOrigin(const GMlib::Point<float, 3> &origin)
{
    _origin = origin;
}

float Plane::friction() const
{
    return _friction;
}

void Plane::setFriction(float friction)
{
    _friction = friction;
}
