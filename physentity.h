#ifndef PHYSENTITY_H
#define PHYSENTITY_H


#include <gmSceneModule>

class Ball;
class Plane;
class Triangle;

class PhysEntity
{
public:
    bool staticObject() const;
    void setStaticObject(bool s);

    virtual bool collide(const PhysEntity* pe, float& t) const = 0;
    virtual void impact(PhysEntity* pe) = 0;

    virtual bool collideV(const Ball* ball, float& t) const = 0;
    virtual bool collideV(const Plane* plane, float& t) const = 0;
    virtual bool collideV(const Triangle* triangle, float& t) const = 0;

    virtual void impactV(Ball* ball) const = 0;
    virtual void impactV(Plane* plane) const = 0;
    virtual void impactV(Triangle* triangle) const = 0;

    virtual void update(float dt) = 0; //Pure virtual
    virtual void applyGravity(float dt) = 0;

private:
    bool _staticObject = true;
};

#endif
