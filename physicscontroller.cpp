
#include "physicscontroller.h"

PhysicsController::PhysicsController()
{

}

PhysicsController::~PhysicsController()
{

}

void PhysicsController::addEntity(PhysEntity* e)
{
    if (e->staticObject() == false)
        _dynamic_objects.emplace_back(e);
    else _static_objects.emplace_back(e);
}

void PhysicsController::clear()
{
    _collisions.clear();

}

bool PhysicsController::detect(double dt)
{
    clear();
    _timespan = dt;
    _x = 0;

    float t;
    for (auto o1: _dynamic_objects)
    {
        for (auto o2: _dynamic_objects)
        {
           if (o2->collide(o1.get(), t) && t<= _timespan)
               _collisions.emplace_back(t, o1, o2);
        }
        for (auto o2: _static_objects)
        {
            if (o2->collide(o1.get(), t) && t<=_timespan)
            {
                _collisions.emplace_back(t, o1, o2);
            }

        }
    }

    std::sort(_collisions.begin(), _collisions.end());

    return true;
}

void PhysicsController::handleFirst()
{
    auto e = _collisions.front();
    float x = e.getX();

    for (auto o: _dynamic_objects) o->update(x-_x);

    _x = x;

    std::shared_ptr<PhysEntity> obj[2] = {e.first.lock(),e.second.lock()};
    if (obj[0] != nullptr && obj[1] != nullptr)
    obj[0]->impact(obj[1].get());

    _collisions.clear();
/*
    float t = 0;
    for(auto o1: _dynamic_objects)
    {
/*
        for(auto o2: _dynamic_objects)
        {
            if(o1==o2) continue;
            if(o1->collide(o2.get(),t) && x+t<=this->_timespan)
            _collisions.emplace_back(t,o1,o2);
        }
       */
/*

        for(auto o2: _static_objects) {
            if(o1->collide(o2.get(),t) && x+t<=this->_timespan){};
            std::cout << "!!! " << t << std::endl;
            //_collisions.emplace_back(t,o1,o2);
        }

    }
*/


    std::sort(_collisions.begin(), _collisions.end());
}

void PhysicsController::doFinalize()
{
    for(auto o: _dynamic_objects) o->update(_timespan-_x);
    for(auto o: _dynamic_objects) o->applyGravity(_timespan);
}

double PhysicsController::getFirstX() const
{
    if (_collisions.empty() == true) return 0;
    return _collisions.front().getX();
}
