#include "ball.h"
#include "collision.h"

Ball::Ball(const GMlib::Point<float, 3>&position,
           const GMlib::Vector<float, 3> &velocity,
           float radius,
           float mass)
    : GMlib::PSphere<float>(radius), _velocity(velocity), _mass(mass)
{
    setStaticObject(false);
    this->translate(position);
}

Ball::~Ball()
{

}

bool Ball::collide(const PhysEntity *pe, float &t) const
{
    return pe->collideV(this, t);
}

void Ball::impact(PhysEntity *pe)
{
    pe->impactV(this);
}

void Ball::update(float dt)
{
    this->translate(dt * _velocity);
}

bool Ball::collideV(const Ball *ball, float &t) const
{
    return Collision::collide(this, ball, t);
}

bool Ball::collideV(const Plane *plane, float &t) const
{
    return Collision::collide(this, plane, t);

}

bool Ball::collideV(const Triangle *triangle, float &t) const
{

}

void Ball::impactV(Ball *ball) const
{

}

void Ball::impactV(Plane *plane) const
{

}

void Ball::impactV(Triangle *triangle) const
{

}

void Ball::applyGravity(float dt)
{
    if (!_rolling)
    _velocity += Collision::gravity * dt;
}

GMlib::Point<float, 3> Ball::velocity() const
{
    return _velocity;
}

void Ball::setVelocity(const GMlib::Point<float, 3> &velocity)
{
    _velocity = velocity;
}

GMlib::Point<float, 3> Ball::position() const
{
   return this->getPos();
}

float Ball::mass() const
{
    return _mass;
}

void Ball::setMass(float mass)
{
    _mass = mass;
}

bool Ball::rolling() const
{
    return _rolling;
}

void Ball::setRolling(bool rolling)
{
    _rolling = rolling;
}

float Ball::friction() const
{
    return _friction;
}

void Ball::setFriction(float friction)
{
    _friction = friction;
}
