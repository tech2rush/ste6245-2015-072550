#ifndef COLLISIONEVENT_H
#define COLLISIONEVENT_H

#include <memory>
#include <scene/gmevent>

#include "physentity.h"

class CollisionEvent : public GMlib::Event {
public:
  explicit CollisionEvent(){}
  explicit CollisionEvent(double t,
                 const std::shared_ptr<PhysEntity> entity_1,
                 const std::shared_ptr<PhysEntity> entity_2):
      GMlib::Event(t), first(entity_1), second(entity_2)
  {}

  std::weak_ptr<PhysEntity> first, second;
};
#endif
